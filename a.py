import re
import os
import numpy as np

variable_length_resistors = ['R025']
variable_length_diodes = ['D300','DIODE']

def convert_fp_to_EIR(fpfile):

    f=open(fpfile)

    elem_str = f.readlines()
    f.close()

    # first pass, found translation to project to 100 mils grid
    in_elem=False
    pins_mils=[]
    pins_id=[]

    for l in elem_str:
        if re.match(".*Element\s*(\(|\[)",l):
            in_elem=True
        if in_elem and re.match("[^\(]*\)",l):
            in_elem=False

        if in_elem:
            line_is_pin = False
            m = re.match("\s*Pin\s*\((-?\d+)\s+(-?\d+)\s+",l)
            if m:
                line_is_pin=True
                # pin in mils
                xy = float(m.groups()[0]),float(m.groups()[1])
                pins_mils.append(xy)

            m = re.match("\s*Pin\s*\[(-?\d+\.?\d*)((?:mil|mm)?)\s+(-?\d+\.?\d*)((?:mil|mm)?)\s+",l)
            if m:
                line_is_pin=True
                # pin in 1/100th mils if group unit not present
                x = float(m.groups()[0])/100
                y = float(m.groups()[2])/100
                if m.groups()[1] == "mil":
                    x=x*100
                if m.groups()[3] == "mil":
                    y=y*100
                if m.groups()[1] == "mm":
                    x=x*100*100/2.54
                if m.groups()[3] == "mm":
                    y=y*100*100/2.54
                pins_mils.append((x,y))

            if line_is_pin:
                m=re.match(".*\s[\"']([^\"']+)[\"']\s+\S+\s*(?:\)|\])\s*$",l)
                if not m:
                    print "not labeled pin"
                    raise

                pins_id.append(m.groups()[0])

    if len(pins_mils) == 0:
        print "No pin footprint: %s" % fpfile
        raise

    # in 100 mils grid unit
    min_x = min(map(lambda x: x[0],pins_mils))
    min_y = min(map(lambda x: x[1],pins_mils))

    fr = lambda a,b: (a-b)/100 - round((a-b)/100)
    offset_x = -11.111 + min_x
    for i in range(100):
        offset_x = offset_x + 100*np.mean(map(lambda x: fr(x[0],offset_x),pins_mils))
    
    offset_y = -11.111 + min_y
    for i in range(100):
        offset_y = offset_y + 100*np.mean(map(lambda x: fr(x[1],offset_y),pins_mils))

    pins = []
    for i in range(len(pins_mils)):
        x = int(round((pins_mils[i][0]-offset_x)/100))
        y = int(round((pins_mils[i][1]-offset_y)/100))
        pid = pins_id[i]

        pins.append((pid,x,y))

    pins = tuple(pins)

    space = set()

    m_x = min(map(lambda x: x[1],pins))
    M_x = max(map(lambda x: x[1],pins))
    m_y = min(map(lambda x: x[2],pins))
    M_y = max(map(lambda x: x[2],pins))

    for x in range(m_x,M_x+1):
        for y in range(m_y,M_y+1):
            space.add((x,y))

    return (pins,space)

def variable_length_EIR_resistor_of_size(n):
    pins = ( ('1',0,0), ('2',n,0) )
    space = set()
    for i in range(0,n+1):
        space.add( (i,0) )
    return (pins,space)

def variable_length_EIR_diode_of_size(n):
    return variable_length_EIR_resistor_of_size(n)

def variable_length_EIR(what):
    if what == 'RESISTOR':
        EIRs = []
        for i in range(1,9):
            EIRs.append( variable_length_EIR_resistor_of_size(i) )
        return EIRs
    if what == 'DIODE':
        EIRs = []
        for i in range(1,9):
            EIRs.append( variable_length_EIR_diode_of_size(i) )
        return EIRs
    raise

        
    

def conv_EIR_to_CEIR(EIR):
    return EIR

def rot_CEIR(CEIR):
    new_CEIR_pins = []
    new_CEIR_space = set()

    for p in CEIR[0]:
        new_CEIR_pins.append( (p[0],p[2],-p[1]) )

    for s in CEIR[1]:
        new_CEIR_space.add( (s[1],-s[0]) )

    new_CEIR_pins = tuple(new_CEIR_pins)
    new_CEIR = (new_CEIR_pins, new_CEIR_space)
    return new_CEIR

def conv_CEIR_to_ACEIR(CEIR):
    A = [CEIR]
    A.append(rot_CEIR(A[-1]))
    A.append(rot_CEIR(A[-1]))
    A.append(rot_CEIR(A[-1]))
    return A
    
def read_components_from_one_schematic(fname):
    f=open(fname)

    in_block = False

    components = {}
    attributes = {}
    for l in f.readlines():
        if re.match('{',l):
            in_block=True
        if in_block and re.match('}',l):
            if 'refdes' in attributes.keys():
                rd = attributes['refdes']
                if not rd in components.keys():
                    components[rd] = {}
                
                if 'footprint' in attributes.keys():
                    components[rd]['footprint'] = attributes['footprint']

            in_block=False
            attributes = {}

        if in_block:
            m = re.match('([a-z]+)=(.*)$',l)
            if m:
                attributes[m.groups()[0]] = m.groups()[1]
    f.close()
    return components

def read_components_from_schematics(fnames):
    components = {}

    for fname in fnames:
        new_components = read_components_from_one_schematic(fname)
        for rd in new_components.keys():
            if rd in components.keys():
                components[rd].update(new_components[rd])
            else:
                components[rd]=new_components[rd]
    return components

def associate_EIR_of_components(components,paths):
    footprints = {}
    for path in paths:
        for root, dirs, files in os.walk(path):
            for f in files:
                m = re.match("(.*).fp$",f)
                if m:
                    footprint = m.groups()[0]
                    complete_file = os.path.join(root,f)
                    footprints[footprint] = complete_file

    for rd in components.keys():
        if not 'footprint' in components[rd].keys():
            print "Component %s: no footprint attribute found" % rd
            raise

        if components[rd]['footprint'] in variable_length_resistors:
            components[rd]['EIRs'] = variable_length_EIR('RESISTOR')
            components[rd]['priority'] = 0
            continue
        
        if components[rd]['footprint'] in variable_length_diodes:
            components[rd]['EIRs'] = variable_length_EIR('DIODE')
            components[rd]['priority'] = 0
            continue

        if not components[rd]['footprint'] in footprints.keys():
            print "Component %s: footprint '%s' not found" % (rd, components[rd]['footprint'])
            raise

        components[rd]['EIRs'] = [ convert_fp_to_EIR(footprints[components[rd]['footprint']]) ]
        components[rd]['priority'] = len(components[rd]['EIRs'][0][0]) + len(components[rd]['EIRs'][0][1])

    return None

def associate_CEIR_of_components(components):
    for rd in components.keys():
        components[rd]['CEIRs'] = {}
        for eir_id in range(len(components[rd]['EIRs'])):
            ceirs = conv_CEIR_to_ACEIR(conv_EIR_to_CEIR(components[rd]['EIRs'][eir_id]))
            for ceir_id in range(len(ceirs)):
                components[rd]['CEIRs'][ (eir_id,ceir_id) ] = ceirs[ceir_id]
    return None

def load_netlist(netfile):
    f = open(netfile)
    nets = []

    lines = []
    last_not_finished = False
    for l in f.readlines():
        if l[-1] == '\n':
            l=l[:-1]
        this_not_finished = False
        if l[-1] == '\\':
            this_not_finished = True
            l = l[:-2]

        if last_not_finished:
            lines[-1] = lines[-1] + l
        else:
            lines.append(l)

        last_not_finished = this_not_finished
    f.close()


    for l in lines:
        m = re.match('.*\t(.*)$',l)
        if not m:
            print "Error reading netfile"
            raise

        net = map(lambda x: tuple(x.split('-')),m.groups()[0].split(' '))

        net = filter(lambda x: len(x[0])>0,net)
        nets.append(net)


    pin_net={}
    for i in range(len(nets)):
        for p in nets[i]:
            pin_net[p] = i


    return (nets,pin_net)

def placement_net_of_CEIR(rd,CEIR,where,netlist):
    placement = {}
    for p in CEIR[0]:
        n = netlist[1][(rd,p[0])]
        pin_loc = (p[1]+where[0],p[2]+where[1])
        placement[pin_loc] = n
    return placement

def placement_space_of_CEIR(CEIR,where):
    sp=set()
    for e in CEIR[1]:
        sp.add((e[0]+where[0],e[1]+where[1]))
    return sp

def valid_placement_net(complete, placement_net, placement_space, xylim):

    cuts = []
    cutid = 0
    bands = {}

    for cutter_row in range(xylim[0]):
        last_cutid=None
        net = None
        start_band = 0
        last_pin_shown_on_net = None
        
        for cutter_col in range(xylim[1]):
            if (cutter_row,cutter_col) in placement_net.keys():
                if net is None:
                    net = placement_net[ (cutter_row,cutter_col) ]
                    last_pin_shown_on_net = cutter_col
                else:
                    if net == placement_net[ (cutter_row,cutter_col) ]:
                        last_pin_shown_on_net = cutter_col
                    else:
                        if cutter_col-last_pin_shown_on_net <=1:
                            if complete:
                                return []
                            else:
                                return False
                        else:
                            if not last_cutid is None:
                                cuts_of_band = (last_cutid,cutid)
                            else:
                                cuts_of_band = (cutid,)
                            if not bands.has_key(net):
                                bands[net] = []
                            bands[net].append((cutter_row,start_band,last_pin_shown_on_net,cuts_of_band))
                            cuts.append((cutter_row,last_pin_shown_on_net+1,cutter_col-1))
                            last_cutid = cutid
                            cutid = cutid + 1
                            net = placement_net[ (cutter_row,cutter_col) ]
                            last_pin_shown_on_net = cutter_col
                            start_band = cutter_col

        if not net is None:
            if not bands.has_key(net):
                bands[net] = []
            if last_cutid is None:
                bands[net].append((cutter_row,start_band,cutter_col,()))
            else:
                bands[net].append((cutter_row,start_band,cutter_col,(last_cutid,)))

    # ok, now we add wire
    to_connect = []
    for net in bands.keys():
        if len(bands[net])>1:
            to_connect.append(bands[net])
    
    # computing cuts free hole
    cuts_free_holes = []
    for c in cuts:
        fh = []
        for col in range(c[1],c[2]+1):
            if not (c[0],col) in placement_space:
                fh.append((c[0],col))
        cuts_free_holes.append(fh)

    # computing band free_holes
    bands_to_connect_free_holes = []
    for bands_to_connect in to_connect:
        bfh = []
        for b in bands_to_connect:
            fh=[]
            for col in range(b[1],b[2]+1):
                if not (b[0],col) in placement_space:
                    fh.append((b[0],col))
            bfh.append(fh)
        bands_to_connect_free_holes.append(bfh)


    # first we look if necessary condition is met
    for bnetid in range(len(bands_to_connect)):
        K = []
        for bid in range(len(bands_to_connect[bnetid])):
            r = len(bands_to_connect_free_holes[bnetid][bid])
            if
    return (bands_to_connect_free_holes, cuts_free_holes)

            

def valid_placement_neto(complete, placement_net, space, xylim, cuts=[],cutter=(0,0),net_bands={}):
    last_pin_shown_on_net = None
    net = None

    cutter_row = cutter[0]
    cutter_col = cutter[1]
    starting_band_col = cutter[1]

    res = []

    while cutter_row <= xylim[0]:
        while cutter_col <= xylim[1]:
            if (cutter_row,cutter_col) in placement_net.keys():
                if net is None:
                    net = placement_net[ (cutter_row,cutter_col) ]
                    last_pin_shown = cutter_col
                else:
                    if net == placement_net[ (cutter_row,cutter_col) ]:
                        last_pin_shown = cutter_col
                    else:
                        # we have to cut between last_pin_shown and here
                        for cut_col in range(last_pin_shown+1,cutter_col):
                            new_cuts = cuts[:]
                            new_net_bands = {}
                            for k in net_bands.keys():
                                new_net_bands[k] = net_bands[k][:]
                            new_cuts.append((cutter_row,cut_col))
                            if not net in net_bands.keys():
                                new_net_bands[net] = []
                            new_net_bands[net].append((cutter_row,starting_band_col,cut_col-1))
                            cut_valid = valid_placement_net(complete,placement_net,space,xylim,new_cuts,
                                    (cutter_row,cut_col+1),new_net_bands)
                            if cut_valid:
                                if not complete:
                                    return True
                                else:
                                    res.extend(cut_valid)
                        if complete:
                            return res
                        else:
                            return False

                        return False
            cutter_col = cutter_col + 1
        if not net is None:
            if not net in net_bands.keys():
                net_bands[net] = []
            net_bands[net].append((cutter_row,starting_band_col,cutter_col-1))
        cutter_row = cutter_row + 1
        cutter_col = 0
        starting_band_col = 0
        last_pin_shown_on_net = None
        net = None

    # if we are here, means all cuts can be done, now, we will try to add wires
    nb_wires = 0
    for n in net_bands.keys():
        if len(net_bands[n])>1:
            holes = []
            for k in range(len(net_bands[n])):
                band=net_bands[n][k]
                nb_holes = 0
                for t in range(band[1],band[2]+1):
                    if not (band[0],t) in space:
                        nb_holes = nb_holes + 1
                holes.append(nb_holes)

            # ok we have a vector of holes
            if min(holes)==0:
                if complete:
                    return []
                else:
                    return False
            if sum(holes) < 2*(len(holes)-1):
                if complete:
                    return []
                else:
                    return False
            nb_wires = nb_wires + len(holes)-1
    
    return [(cuts,nb_wires)]


def border(xylim):
    border = set()
    for x in range(-1,xylim[0]+2):
        border.add( (x,-1) )
        border.add( (x,xylim[1]+1) )
    for y in range(-1,xylim[1]+2):
        border.add( (-1,y) )
        border.add( (xylim[0]+1,y) )
    return border

def add_components(components,netlist,bord,xylim,order,pos,placement_space,placement_net,placed_comp):

    if pos == len(order):
        return placed_comp

    print "Proposal: %s" % order[pos]
    complete_placed_comp = []
    CEIRs_comp = components[order[pos]]['CEIRs']
    for x in range(xylim[0]+1):
        for y in range(xylim[1]+1):

            print "Proposal: %s in (%d,%d)" % (order[pos],x,y)
            for ceir_id in CEIRs_comp.keys():
                ceir = CEIRs_comp[ceir_id]
                placement_space_ceir = placement_space_of_CEIR(ceir,(x,y))
                if not bord.isdisjoint(placement_space_ceir):
                    continue
                if not placement_space.isdisjoint(placement_space_ceir):
                    continue
                new_placement_space = placement_space.union(placement_space_ceir)
                placement_net_ceir = placement_net_of_CEIR(order[pos],ceir,(x,y),netlist)
                new_placement_net = placement_net.copy()
                new_placement_net.update(placement_net_ceir)
                if not valid_placement_net(False, new_placement_net, new_placement_space, xylim):
                    continue

                new_placed_comp = placed_comp[:]
                new_placed_comp.append( (order[pos],ceir_id,(x,y)) )

                complete_placed_comp.extend(add_components(components,netlist,bord,xylim,order,pos+1,new_placement_space,new_placement_net,new_placed_comp))

        if len(complete_placed_comp)==0:
            print "Dead end: %s" % order[pos]
        return complete_placed_comp

def do_the_jobs(components, xylim, netlist):
    bord = border(xylim)

    ordered = map(lambda x: (x,components[x]['priority']), components.keys())
    ordered.sort(lambda x,y: cmp(x[1],y[1]),reverse=True)
    order = map(lambda x: x[0], ordered)

    print order
    return add_components(components,netlist,bord,xylim,order,0,set(),{},[])









            










